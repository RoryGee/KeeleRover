import Tkinter as tk
import socket
import time
import threading

prevKeys = []
#var = tk.StringVar()

def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])

def keyRelease(event):
    print event.keycode
    if  event.keycode in prevKeys:
        prevKeys.pop(prevKeys.index(event.keycode))
    
def keyPress(event):
    if not event.keycode in prevKeys:
        prevKeys.append(event.keycode)
    
class App():
    def __init__(self):
        self.root = tk.Tk()
        self.started = False
        p = tk.IntVar()
        E_port = tk.Entry(self.root,textvariable=p)
        E_port.delete(0)
        E_port.insert(0, "8097")
        E_port.pack()
        B_start = tk.Button(self.root, text ="Start server", command =lambda : self.button_start(p))
        B_start.pack()
        self.root.bind("<KeyPress>", keyPress)
        self.root.bind("<KeyRelease>", keyRelease)

    def button_start(self,p):
        self.PORT = p.get()
        self.started = True
        inputs.start()
        conn.start()

class connHandler(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        if app.started == True:
            s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            addr = ('', app.PORT)
            s.bind(addr)
            s.listen(1)
            print("I'm trying")
            packet, addr = s.accept()
            while 1:
                print(inputs.curPacket)
                packet.sendall(inputs.curPacket)
                time.sleep(0.3)

class inputHandler(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.servoSpeed = 0.00
        self.armSpeed = 0.00

    def formatPacket(self):
        packetData = ''
        if 37 in prevKeys:
            print("left")
            packetData = packetData+'1 '
        elif 38 in prevKeys:
            print("up")
            packetData = packetData+'2 '
        elif 39 in prevKeys:
            print("right")
            packetData = packetData+'3 '
        elif 40 in prevKeys:
            print("down")
            packetData = packetData+'4 '
        elif 27 in prevKeys:
            packetData = packetData+'9 '
        else:
            packetData = packetData+'0 '
        if 107 in prevKeys:
            if self.servoSpeed <= 99.50:
                self.servoSpeed = self.servoSpeed + 0.50
        elif 109 in prevKeys:
            if self.servoSpeed >= 0.50:
                self.servoSpeed = self.servoSpeed - 0.50
        if 219 in prevKeys:
            if self.armSpeed <= 99.50:
                self.armSpeed = self.armSpeed + 0.50
        elif 221 in prevKeys:
            if self.armSpeed >= 0.50:
                self.armSpeed = self.armSpeed - 0.50            
        packetData = packetData + truncate(self.servoSpeed, 2)+ " " + truncate(self.armSpeed, 2)
        while len(packetData) < 16:
            packetData = packetData+' '
        return packetData
    def run(self):
        while 1:
            self.curPacket=self.formatPacket()
            time.sleep(0.1)
    
app = App()
conn = connHandler()
inputs = inputHandler()
tk.mainloop()
