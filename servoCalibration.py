print("Project Johnson servo calibration tool")

try:
	import Adafruit_BBIO.PWM as PWM
except RuntimeError:
	print "Error importing Adafruit_BBIO.PWM, ensure you have enabled superuser privileges (run with sudo!)"

print("Initialising states...")

fullLeftLeft = 5.
middleLeft = 7.5
fullRightLeft = 10.

fullLeftRight = 5.
middleRight = 7.5
fullRightRight = 10.

increments = 0.25

print("Starting PWM pins...")

leftServo = "P9_14"
rightServo = "P9_16"

PWM.start(leftServo,middleLeft,50)
PWM.start(rightServo,middleRight,50)

keyInput = 'n'
adjustmentInput = ''

print("Let's start callibrating!")

while keyInput == 'n':
    PWM.set_duty_cycle(leftServo,middleLeft)
    keyInput = input("Is servo 1 in the center position? (y/n): ")
    if keyInput == 'n':
        adjustmentInput = input("Too far left or too far right? (l/r): ")
        if adjustmentInput == 'l':
            middleLeft = middleLeft + increments
        else if adjustmentInput == 'r':
            middleLeft = middleLeft - increments
        else:
            print('Unaccepted input, try again')
    elif keyInput == 'y':
        print("Awesome, let's try a different state...")
    else:
        print('Unaccepted input, try again')
        keyInput = 'n'

fullLeftLeft = middleLeft - 2.5
keyInput = 'n'

while keyInput == 'n':
    PWM.set_duty_cycle(leftServo,fullLeftLeft)
    keyInput = input("Is servo 1 in the far left position? (y/n): ")
    if keyInput == 'n':
        adjustmentInput = input("Too far left or too far right? (l/r): ")
        if adjustmentInput == 'l':
            fullLeftLeft = fullLeftLeft + increments
        else if adjustmentInput == 'r':
            fullLeftLeft = fullLeftLeft - increments
        else:
            print('Unaccepted input, try again')
    elif keyInput == 'y':
        print("Awesome, let's try a different state...")
    else:
        print('Unaccepted input, try again')
        keyInput = 'n'

fullRightLeft = fullLeftLeft - 5.
keyInput = 'n'

while keyInput == 'n':
    PWM.set_duty_cycle(leftServo,fullRightLeft)
    keyInput = input("Is servo 1 in the far right position? (y/n): ")
    if keyInput == 'n':
        adjustmentInput = input("Too far left or too far right? (l/r): ")
        if adjustmentInput == 'l':
            fullRightLeft = fullRightLeft + increments
        else if adjustmentInput == 'r':
            fullRightLeft = fullRightLeft - increments
        else:
            print('Unaccepted input, try again')
    elif keyInput == 'y':
        print("Awesome, let's try the other servo...")
    else:
        print('Unaccepted input, try again')
        keyInput = 'n'

print("Switching to the second servo...")
keyInput = 'n'

while keyInput == 'n':
    PWM.set_duty_cycle(rightServo,middleRight)
    keyInput = input("Is servo 1 in the center position? (y/n): ")
    if keyInput == 'n':
        adjustmentInput = input("Too far left or too far right? (l/r): ")
        if adjustmentInput == 'l':
            middleRight = middleRight + increments
        else if adjustmentInput == 'r':
            middleRight = middleRight - increments
        else:
            print('Unaccepted input, try again')
    elif keyInput == 'y':
        print("Awesome, let's try a different state...")
    else:
        print('Unaccepted input, try again')
        keyInput = 'n'

fullLeftRight = middleRight - 2.5
keyInput = 'n'

while keyInput == 'n':
    PWM.set_duty_cycle(rightServo,fullLeftRight)
    keyInput = input("Is servo 1 in the far left position? (y/n): ")
    if keyInput == 'n':
        adjustmentInput = input("Too far left or too far right? (l/r): ")
        if adjustmentInput == 'l':
            fullLeftRight = fullLeftRight + increments
        else if adjustmentInput == 'r':
            fullLeftRight = fullLeftRight - increments
        else:
            print('Unaccepted input, try again')
    elif keyInput == 'y':
        print("Awesome, let's try a different state...")
    else:
        print('Unaccepted input, try again')
        keyInput = 'n'

fullRightRight = fullLeftRight - 5.
keyInput = 'n'

while keyInput == 'n':
    PWM.set_duty_cycle(rightServo,fullRightRight)
    keyInput = input("Is servo 1 in the far right position? (y/n): ")
    if keyInput == 'n':
        adjustmentInput = input("Too far left or too far right? (l/r): ")
        if adjustmentInput == 'l':
            fullRightRight = fullRightRight + increments
        else if adjustmentInput == 'r':
            fullRightRight = fullRightRight - increments
        else:
            print('Unaccepted input, try again')
    elif keyInput == 'y':
        print("Awesome, let's look at the results...")
    else:
        print('Unaccepted input, try again')
        keyInput = 'n'

print("Processing results...")

keyInput = 'n'

leftIncr = ((180-0)/(fullRightLeft - fullLeftLeft))
rightIncr = ((180-0)/(fullRightRight - fullLeftRight))

calcMiddleLeft = ((fullLeftLeft+fullRightLeft)/2)
calcMiddleLeft = ((fullLeftRight+fullRightRight)/2)

while keyInput == 'n':
    print("Servo 1...")
    print("Experimental center: %.2f" % middleLeft)
    print("Calculated center: %.2f" % calcMiddleLeft)
    print("Far left: %.2f" % fullLeftLeft)
    print("Far right: %.2f" % fullRightLeft)
    print("Increment for 1 degree: %.2f" % leftIncr)
    print("")
    print("Servo 2...")
    print("Experimental center: %.2f" % middleRight)
    print("Calculated center: %.2f" % calcMiddleRight)
    print("Far left: %.2f" % fullLeftRight)
    print("Far right: %.2f" % fullRightRight)
    print("Increment for 1 degree: %.2f" % rightIncr)   
    
    keyInput = input("Exit the application? (y/n): ")

print("Stopping PWM pins...")

PWM.stop(leftServo)
PWM.stop(rightServo)

print("Cleaning up pins...")

PWM.cleanup()

print("Goodbye")
