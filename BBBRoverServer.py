#!usr/bin/python
# KRIS/C Server

import config, socket
from time import sleep

from util import *
from Objects import *

try:
	import Adafruit_BBIO.GPIO as GPIO
except RuntimeError:
	print "Error importing Adafruit_BBIO.PWM, ensure you have enabled superuser privileges (run with sudo!)"
try:
	import Adafruit_BBIO.PWM as PWM
except RuntimeError:
	print "Error importing Adafruit_BBIO.PWM, ensure you have enabled superuser privileges (run with sudo!)"

print "--- PROJECT JOHNSON ---"
print "KRIS/C SERVER ACTIVATED"

# Variable initialisation for networking
s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
i = 0
while True:
	try:
		addr = (config.HOST, config.PORT+i)
		s.bind(addr)
		break
	except socket.error:
		print "SOCKET ({}) IS IN USE. TRYING NEXT AVAILABLE SOCKET.".format(config.PORT+i)
		i = i + 1


# Initialise Motors
leftMotor = Motor(config.MOTORPINS['AForward'], config.MOTORPINS['AReverse'])
rightMotor = Motor(config.MOTORPINS['BForward'], config.MOTORPINS['BReverse'])

print "Socket initialised"
print "Listening on: {} ({})".format(config.HOST, config.PORT+i)

# Recieving packets indefinitely

running = True
while running:
	try:
		s.listen(1)
		conn, addr = s.accept()
		print "Client (", addr[0], ") connected"

		connected=True

		while connected:
			data=conn.recv(16) # Todo: Add additional serial cmd + BBIO pin for direction for servo?
			try:
				buf = parseCommand(data)
				print"Serial command:",buf

				if buf == 'left': # Left
					leftMotor.goBackwards()
					rightMotor.goForwards()
				elif buf == 'forwards': # Forwards
					leftMotor.goForwards()
					rightMotor.goForwards()
				elif buf == 'right': # Right
					leftMotor.goForwards()
					rightMotor.goBackwards()
				elif buf == 'backwards': # Backwards
					leftMotor.goBackwards()
					rightMotor.goBackwards()
				elif buf=='softBrake': # Stationary
					leftMotor.brake()
					rightMotor.brake()
				elif buf=='disconnect': # Server disconnected
					print "Client disconnected."
					conn.close()
					connected=False
				elif buf=='kill':
					print "Client disconnected."
					connected=False
					running=False					
			except:
				pass
	except KeyboardInterrupt:
		break

print "KRIS/C BEGINNING SHUTDOWN SEQUENCE"
s.shutdown(socket.SHUT_RDWR)
s.close()
print "Socket terminated. Beginning GPIO clean."
GPIO.cleanup()
print "GPIO cleaned. Beginning PWM clean."
PWM.cleanup()
print "PWM cleaned."

print "KRIS/C SERVER DEACTIVATED"
print "--- PROJECT JOHNSON ---"

#Fin
