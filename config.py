HOST='0.0.0.0'
PORT=8097

MOTORPINS={'AForward': 'P8_7', 'AReverse': 'P8_9', 'ASpeed': 'P9_14',
'BForward': 'P8_11', 'BReverse': 'P8_15', 'BSpeed': 'P9_16'}

COMMANDS = {1: 'left', 2:'forwards', 3:'right', 4:'backwards', 0:'softBrake', 9:'disconnect', 'k':'kill'}