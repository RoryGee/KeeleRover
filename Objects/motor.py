try:
	import Adafruit_BBIO.GPIO as GPIO
except RuntimeError:
	print "Error importing Adafruit_BBIO.PWM, ensure you have enabled superuser privileges (run with sudo!)"
try:
	import Adafruit_BBIO.PWM as PWM
except RuntimeError:
	print "Error importing Adafruit_BBIO.PWM, ensure you have enabled superuser privileges (run with sudo!)"

class Motor:

    forwardsPin=0
    backwardsPin=0

    # Flip allows for the pins to be swapped without changing wires i.e. if wires are the wrong way round.

    def __init__(self, forwardsPin, backwardsPin, pwmSpeedPin=None, flip=False):
        GPIO.setup(forwardsPin, GPIO.OUT)
        GPIO.setup(backwardsPin, GPIO.OUT)
        if pwmSpeedPin is not None:
            raise Exception('PWM Speed Control is not implemented')

        if flip:
            self.forwardsPin = backwardsPin
            self.backwardsPin = forwardsPin
        else:
            self.forwardsPin = forwardsPin
            self.backwardsPin = backwardsPin

    def goForwards(self):
        GPIO.output(self.backwardsPin, GPIO.LOW)
        GPIO.output(self.forwardsPin, GPIO.HIGH)

    def goBackwards(self):
        GPIO.output(self.backwardsPin, GPIO.LOW)
        GPIO.output(self.forwardsPin, GPIO.HIGH)

    def brake(self, softBrake=True):
        if softBrake:
            GPIO.output(self.backwardsPin, GPIO.LOW)
            GPIO.output(self.forwardsPin, GPIO.LOW)
        else:
            raise Exception('Hard brake not implemented')
