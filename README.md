# Keele Rover Instructional Server/Client (KRIS/C)

KRIS/C is a simple TCP server and client designed specifically for the Keele Lunar Rover, as part of Keele's entry into the UKSEDS Rover competition, designed in Python!

## Server

The server is designed for use with the BeagleBone Black Wireless, but with simple modification can also be used with other single board computers such as the Raspberry Pi, the server uses the Adafruit BBIO module which is included on all standard BeagleBone Linux distributions, in this instance the BBBW runs Debian Wheezy.

The server is, put simply; A socket based TCP channel accepting padded 16 bit commands, mapping said commands to GPIO output to allow for tank-esque directional movement and control of a single pwm channel for a peripheral (i.e. a sample collection unit, or alternatively a rail gun).

## Client
The client is being redesigned around the defacto standard Tkinter event-driven GUI framework, as such this portion of the project is undergoing major refactoring, in addition to existing features the client will also eventually:
- Support a handful of XInput devices such as Xbox 360 controllers and single/dual button joysticks
- Either as part of the existing client or as a seperate codebase, support text commands for educational use

## "But what about the webcam, Rory?"
The steaming service uses MJPEG-streamer, and is already configured to stream over port 8090. To view the stream, identify the IP address of the BeagleBone (if you don't have networking know-how this can be found on the BeagleBone start page, located on the drive that is mounted when the BB is tethered to a computer with the USB cable) and enter the following into the address bar:
```sh
<ip.of.beagle.bone>:8090/?action=stream
```
Once you have loaded the page, the stream should appear! As far as I know, you can connect several devices to the same stream, however keep in mind that the BeagleBone has very limited resources, so don't go too crazy with them.
The code isn't included here but is already loaded onto the BB, if you experience issues connecting to the webcam stream, I recommend restarting the service by executing the following command in the BB's terminal:
```sh
sudo systemctl restart mjpg-streamer
```
You may be prompted to enter the root password as such:
```sh
[sudo] password for debian: 
```
 I've kept it as the default on the rover's operating system and should be 'temppwd'
## Basic usage

### Client
To run the client, copy the file BBBRoverClient.py to any directly and run with elevated permissions (this is to reduce the risk of Windows Firewall ruining the day), when launched, either type in an open port number or leave as default and press connect, keep an eye on the console as it will update you on the status of the connection.

### Server
To run the server, either SSH into the BeagleBone or connect a HDMI cable and use the terminal to navigate to the file containing RoverServer.py and run with elevated permissions with the following command:
```sh
sudo python BBBRoverServer.py
```
Enter your password to continue to execution, the client should start sending packets and should inform you of this on the client end.